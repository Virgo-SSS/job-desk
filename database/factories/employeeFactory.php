<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class employeeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => 'joko',
            'password' => 'joko',
            'company_id' => 1,
            'city' => 'jakarta',
            'phone' => '0812341234',
            'email' => 'joko@gmail.com',
            'created_users_id' => 1,
            'updated_users_id' => 1,
        ];
    }
}
