<?php

namespace Tests\Feature;

use Faker\Factory;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic feature test example.
     *
     * @return void
     */

     /**
      * @test
      */
    public function going_to_login_page()
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
    }


     /**
      * @test
      */
    public function login_user()
    {
        // make user factory
        $user = User::factory()->create();

        // login user with factory
        $response = $this->post(route('loginuser', [
            'email' => $user->email,
            'password' => 'password'
        ]));
    

        // test user redirect to home after login
        $response->assertRedirect('/home')
        ->assertStatus(302);

    }

     /**
      * @test
      */
    public function login_validation_user_fails_email_missing()
    {
        // make user factory
        $user = User::factory()->create();

        // login user with factory
        $response = $this->post(route('loginuser', [
            'password' => 'password'
        ]));
    

        $response->assertInvalid();

    }

    /**
     * @test
    */
    public function login_validation_user_fails_password_missing()
    {
        // make user factory
        $user = User::factory()->create();

        // login user with factory
        $response = $this->post(route('loginuser', [
            'email' => $user->email,
        ]));
    

        $response->assertInvalid();

    }

    /**
    * @test
    */
    public function login_validation_if_invalid_user_redirect_back_to_login_page()
    {
        // make user factory
        $user = User::factory()->create();

        // login user with factory
        $response = $this->post(route('loginuser', [
            'email' => $user->email,
            'password' => 'pass'
        ]));
    

        $response->assertRedirect('/login');

    }

    /**
    * @test
    */
    public function user_can_logout()
    {
        $response = $this->post(route('logout'));
          

        $response->assertRedirect('/login');
    }  
}
