<?php



use Illuminate\Routing\RouteGroup;
use Illuminate\Contracts\Queue\Job;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\jobcontroller;

use Illuminate\Support\Facades\Session;



use App\Http\Controllers\HomeController;
use App\Http\Controllers\itemController;
use App\Http\Controllers\sellController;
use App\Http\Controllers\loginController;
use App\Http\Controllers\summaryController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\CompaniesController;
use App\Http\Controllers\LocalizationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['auth']], function () {
    
    //route For home
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::get('/home', [HomeController::class, 'index'])->name('home');
    
    // route for companies using companies controller
    Route::get('/companies', [CompaniesController::class, 'index'])->name('companies');
    Route::get('/inputcompanies', [CompaniesController::class, 'create'])->name('inputcomp');
    Route::post('/submitcompanies', [CompaniesController::class, 'store'])->name('submitcomp');
    Route::get('/editcompanies/{id}', [CompaniesController::class, 'edit'])->name('editcomp');
    Route::put('/updatecompanies/{id}', [CompaniesController::class, 'update'])->name('updatecomp');
    Route::get('/companies/{id}', [CompaniesController::class, 'destroy'])->name('deletecomp');
    Route::get('/searchcompanies', [CompaniesController::class, 'search'])->name('searchcomp');
    
    
    // route for employee using employee controller
    Route::get('/employee', [EmployeeController::class, 'index'])->name('employee');
    Route::get('/inputemployee', [EmployeeController::class, 'create'])->name('inputemplo');
    Route::post('/submitemployee', [EmployeeController::class, 'store'])->name('submitemplo');
    Route::get('/editemployee/{id}', [EmployeeController::class, 'edit'])->name('editemplo');
    Route::put('/updateemployee/{id}', [EmployeeController::class, 'update'])->name('updateemplo');
    Route::get('/deleteemployee/{id}', [EmployeeController::class, 'destroy'])->name('deleteemplo');
    Route::get('/searchemployee', [EmployeeController::class, 'search'])->name('searchemplo');
    
    
    // route for item page
    Route::get('/items', [itemController::class, 'index'])->name('item');
    Route::get('/inputitems', [itemController::class, 'create'])->name('inputitems'); // for input item
    Route::post('/submititems', [itemController::class, 'store'])->name('submititems'); // for submit to db item
    Route::get('/edititems/{id}', [itemController::class, 'edit'])->name('edititems'); // for edit to db item
    Route::put('/updateitems/{id}', [itemController::class, 'update'])->name('updateitems'); // for update to db item
    Route::get('/deleteitems/{id}', [itemController::class, 'destroy'])->name('deleteitems'); // for delete to db item
    Route::get('/searchitems', [itemController::class, 'search'])->name('searchitems'); // for search item
    
    // Route for sell page
    Route::get('/sell', [sellController::class, 'index'])->name('sell');
    Route::get('/inputsell', [sellController::class, 'create'])->name('inputsell'); // for input sell
    Route::post('/submitsell', [sellController::class, 'store'])->name('submitsell'); // for input sell
    Route::get('/editsell/{id}', [sellController::class, 'edit'])->name('editsell'); // for input sell
    Route::put('/updatesell/{id}', [sellController::class, 'update'])->name('updatesell'); // for input sell
    Route::get('/deletesell/{id}', [sellController::class, 'destroy'])->name('deletesell'); // for delete sell
    Route::get('/searchsell', [sellController::class, 'search'])->name('searchsell'); // for search sell
    
    
    
    // Route for summary page
    Route::get('/summary', [summaryController::class, 'index'])->name('summary');
    Route::get('/detailsummary/{id}', [summaryController::class, 'detail'])->name('detailsummary'); 
    Route::get('/searchsummary', [summaryController::class, 'search'])->name('searchsummary'); 
    
    
    // Route for mail
    Route::get('test-email', [jobcontroller::class, 'enqueue'])->name('test-email');
    
    
    
    
    // Route for Language
    if (file_exists(app_path('Http/Controllers/LocalizationController.php')))
    {
        Route::get('/locale/{locale}', [LocalizationController::class , 'locale'])->name('locale');
    }
    
    // Route for timezone
    Route::get('tz/{tz}', ['as' => 'tz.switch', 'uses' => App\Http\Controllers\datetimeController::class, 'switch']);
});



// Route for login
Route::get('/login',[loginController::class,'index'])->name('login')->middleware('guest');
Route::post('/loginuser',[loginController::class,'loginuser'])->name('loginuser');
Route::post('/logout',[loginController::class,'logout'])->name('logout');


