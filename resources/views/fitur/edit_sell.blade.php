@extends('layouts.mainform')

@section('formcss')

<?php 
use Carbon\Carbon;
?>


<div class="main-content">
    <div class="form-mini-container item-select">
        <h1 style="text-align: center">{{ trans('companies.updatesell') }}</h1>
        
        <form class="form-mini" method="post" action="{{ route('updatesell', $se->id)}}" enctype="multipart/form-data" >
            {{ csrf_field() }}
            @method('PUT')
            <input type="text" name="created_date" placeholder="date" id="created_date" value="{{ $se->created_date}}" hidden >
            <div class="form-row ">
                <label>
                    <label for="item">{{ trans('companies.items') }}</label>
                    <select name="item" id="item">
                        <option value="{{ $se->item_id }}" >{{ $se->Sitem->name }}</option>
                        @foreach ($items as $item)
                            @if (old('item') == $item->id)
                                <option value="{{ $item->id }}" data-price="{{ $item->price }}" selected>{{ $item->name }}</option>
                            
                            @else 
                                <option value="{{ $item->id }}" data-price="{{ $item->price }}" >{{ $item->name }}</option>
                            @endif
                        @endforeach
                    </select>  
                </label>
            </div>

            <div class="form-row">
                <label for="price">{{ trans('companies.price') }}</label>
                <input type="number" name="price" placeholder="Price" readonly class="price-input">
            </div>
            <div class="form-row">
                <label for="discount">{{ trans('companies.discount') }}(%)</label>
                <input type="number" name="discount" placeholder="Discount" id="discount" required value="{{ $se->discount }}">
            </div>
            <div class="form-row">
                <label>
                <label for="employee_id">{{ trans('companies.employe') }}</label>
                    <select name="employee_id" id="employee_id">
                        <option value="{{ $se->employee_id }}">{{ $se->Semploy->name }}</option>
                        @foreach ($emp as $ep)   
                            @if(old('employee_id') == $ep->id)
                            <option value="{{ $ep->id }}" selected>{{ $ep->name }}</option>
                            @else
                            <option value="{{ $ep->id }}" >{{ $ep->name }}</option>
                            @endif
                          
                        @endforeach
                    </select>
                </label>
            </div>
            <div class="form-row form-last-row">
                <button type="submit">{{ trans('companies.submit') }}</button>
            </div>
        </form>
    </div>

</div>
@endsection

@section('script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
    $('.item-select').on('change', function(){
        $('.price-input')
            .val(
                $(this).find(':selected').data('price')
            );
    });
</script>
@endsection