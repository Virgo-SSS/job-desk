<?php

namespace App\Http\Controllers;

use App\Models\companies;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Stringable;

class datetimeController extends Controller
{
    public function __invoke($tz)
    {   
       $value = str_replace(' ', '/', $tz);
       Session::put('tz', $value);
        return redirect()->back();
    }
}