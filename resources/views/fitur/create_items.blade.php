@extends('layouts.mainform')

@section('formcss')
<div class="main-content">

  <div class="form-mini-container">
    <h1 style="text-align: center">{{ trans('companies.inputitems') }}</h1>

    <form class="form-mini" method="post" action="{{ route('submititems') }}" enctype="multipart/form-data">
      {{ csrf_field() }}
      <div class="form-row">
        <label for="items"><h3>{{ trans('companies.items') }}</h3></label>
        <input type="text" id="items" name="items" placeholder="Items" required>
      </div>
      <div class="form-row">
        <label for="price"><h3>{{ trans('companies.price') }}</h3></label>
        <input type="number" id="price" name="price" placeholder="Price" required>
      </div>
      <div class="form-row form-last-row">
        <button type="submit">{{ trans('companies.submit') }}</button>
      </div>
    </form>
  </div>


</div>
@endsection