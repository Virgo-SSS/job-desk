<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class item extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';
    protected $table = 'items';
    protected $fillable = ['name', 'price'];


    public function Isell()
    {
        return $this->hasMany(sell::class);
    }
}
