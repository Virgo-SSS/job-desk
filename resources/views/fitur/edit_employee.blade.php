@extends('layouts.mainform')

@section('formcss')
<div class="main-content">

  <div class="form-mini-container">
    <h1 style="text-align: center">{{ trans('companies.inputemployee') }}</h1>

    <form class="form-mini" method="post" action="{{ route('updateemplo', $employee->id) }}" enctype="multipart/form-data">
      {{ csrf_field() }}
      @method('PUT')
      <div class="form-row">
        <label for="name"><h3>{{ trans('companies.name') }}</h3></label>
        <input type="text" id="name" name="name" placeholder="Name" required value="{{ old('name', $employee->name) }}">
      </div>
      <div class="form-row">
        <label for="password"><h3>{{ trans('companies.name') }}</h3></label>
        <input type="password" id="password" name="password" placeholder="Password" required value="{{ old('password', $employee->name) }}">
      </div>
      <div class="form-row">
        <label for="company_id"><h3>{{ trans('companies.company') }}</h3></label>
        <select name="company_id" id="company_id">
            <option value="{{ $employee->company_id }}" >{{ $employee->Rcompany->company_name }}</option>
          @foreach ($comp as $cp)
            @if (old('company_id') == $cp->id)
              <option value="{{ $cp->id }}" selected>{{ $cp->company_name }}</option>
            @else
              <option value="{{ $cp->id }}" >{{ $cp->company_name }}</option>
            @endif
          @endforeach
        </select>
      </div>
      <div class="form-row">
        <label for="name"><h3>{{ trans('companies.city') }}</h3></label>
        <input type="text" id="city" name="city" placeholder="City" required value="{{ old('city', $employee->city) }}">
      </div>
      <div class="form-row">
        <label for="phone"><h3>{{ trans('companies.phone') }}</h3></label>
        <input type="number" id="phone" name="phone" placeholder="Phone" required value="{{ old('phone', $employee->phone) }}">
      </div>
      <div class="form-row">
        <label for="email"><h3>{{ trans('companies.email') }}</h3></label>
        <input type="email" id="email" name="email" placeholder="Email" required value="{{ old('email', $employee->email) }}">
      </div>
      <div class="form-row">
        <input type="text" name="created_users_id" hidden value="{{ Auth::id() }}">
      </div>
      <div class="form-row">
        <input type="text" name="updated_users_id" hidden value="{{ Auth::id() }}">
      </div>
      <div class="form-row form-last-row">
        <button type="submit">{{ trans('companies.submit') }}</button>
      </div>
    </form>
  </div>

</div>
@endsection