<?php

namespace Database\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class sellFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'created_date' => $this->faker->dateTime(),
            'item_id' => 3,
            'price' => 122000,
            'discount' => 10,
            'employee_id' => 1,
        ];
    }
}
