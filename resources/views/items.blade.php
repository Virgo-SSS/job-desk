@extends('layouts.main')

@section('lyts')
<h2>{{ trans('companies.listitems') }}</h2>

{{-- CRUD DROPDOWN --}}
<div class="dropdown" aria-labelledby="navbarDropdown" style="float:right; ">
  <button class="dropbtn"><h4>+</h4></button>
  <div class="dropdown-content  dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="float:right; oveflow: auto; max-heiht:200px;">
    <a href="/inputcompanies" class="dropdown-item" >{{ trans('companies.company') }}</a>
    <a href="/inputemployee" class="dropdown-item" >{{ trans('companies.employe') }}</a>
    <a href="/inputitems" class="dropdown-item" >{{ trans('companies.items') }}</a>
    <a href="/inputsell" class="dropdown-item" >{{ trans('companies.sell') }}</a>
  </div>
</div>
{{-- END CRUD DROPDOWN --}}


{{-- SEARCH FILTER --}}
<form action="{{ route('searchitems') }}" method="GET">

  <input type="text" name="name" id="myem"  placeholder="Name" value="{{ request('name') }}">
  <input type="number" name="price" id="myem3"  placeholder="Price" value="{{ request('price') }}">
  <button class="btn btn-primary" type="submit">Search</button>
  
</form>
{{-- END SEARCH FILTER --}}

{{-- TABLE CONTENT --}}
<div class="container">
  <div class="row">
    @if (session()->has('status'))
    <h3 style="text-align: center">
        {{ session('status') }}
    </h3>
        
    @endif
    <div class="col-xs-12">
      <table class="table table-bordered table-hover dt-responsive text-center">
        <thead>
          <tr>
            <th class="bg-primary text-center" >{{ trans('companies.name') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.price') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.action') }}</th>
          </tr>
        </thead>
        <tbody>
            @foreach($items as $item)
            <tr>
                <td>{{ $item->name }}</td>
                <td>{{ number_format($item->price) }}</td>
                <td>
                    <a href="{{ url('edititems',$item->id) }}" class="btn btn-success p-1 mb-1">{{ trans('companies.edit') }}</a>  | <a href="{{ url('deleteitems', $item->id)}}" class="btn btn-warning">{{ trans('companies.delete') }} </a>
                </td>
            </tr>
            @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

<!-- partial -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js'></script>
<script src='https://cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js'></script>
<script src='https://cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.js'></script>
<script  src="../js/script.js"></script>
@endsection