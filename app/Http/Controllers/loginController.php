<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

// use Tymon\JWTAuth\Contracts\Providers\Auth;


class loginController extends Controller
{
    public function index()
    {
        return view('login.login', [
            'title' => 'Login',
        ]);
    }

    public function loginuser(Request $request)
    {
        
        $credentials = $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]); 

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return redirect()->intended('/home');
        }
        return redirect('login')->with('loginerror', 'Login Failed');
       
    }

    public function logout()
    {
        Auth::logout();
        session()->invalidate();
        session()->regenerate();

        return redirect('/login');
    }
}
